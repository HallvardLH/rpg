package no.hallvard.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    @Test
    void init_correctLevelAtInit_ShouldReturn1() {
        // Arrange
        Character mage = new Mage("Carl Riiber");
        int expected = 1;
        int actual = mage.level;

        // Act

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_correctLevelAtLevelUp_ShouldReturn2() {
        // Arrange
        Character mage = new Mage("Carl Riiber");
        mage.levelUp();
        int expected = 2;
        int actual = mage.level;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void init_mageCorrectAttributesAtInit_ShouldReturn5118() {
        // Arrange
        Character mage = new Mage("Carl Riiber");
        int vitalityExpected = 5;
        int vitalityActual = mage.baseAttributes.vitality;
        int strengthExpected = 1;
        int strengthActual = mage.baseAttributes.strength;
        int dexterityExpected = 1;
        int dexterityActual = mage.baseAttributes.dexterity;
        int intelligenceExpected = 8;
        int intelligenceActual = mage.baseAttributes.intelligence;

        // Assert
        assertEquals(vitalityExpected, vitalityActual);
        assertEquals(strengthExpected, strengthActual);
        assertEquals(dexterityExpected, dexterityActual);
        assertEquals(intelligenceExpected, intelligenceActual);
    }

    @Test
    void init_rangerCorrectAttributesAtInit_ShouldReturn8171() {
        // Arrange
        Character ranger = new Ranger("Carl Riiber");
        int vitalityExpected = 8;
        int vitalityActual = ranger.baseAttributes.vitality;
        int strengthExpected = 1;
        int strengthActual = ranger.baseAttributes.strength;
        int dexterityExpected = 7;
        int dexterityActual = ranger.baseAttributes.dexterity;
        int intelligenceExpected = 1;
        int intelligenceActual = ranger.baseAttributes.intelligence;

        // Assert
        assertEquals(vitalityExpected, vitalityActual);
        assertEquals(strengthExpected, strengthActual);
        assertEquals(dexterityExpected, dexterityActual);
        assertEquals(intelligenceExpected, intelligenceActual);
    }

    @Test
    void init_rogueCorrectAttributesAtInit_ShouldReturn8261() {
        // Arrange
        Character rogue = new Rogue("Carl Riiber");
        int vitalityExpected = 8;
        int vitalityActual = rogue.baseAttributes.vitality;
        int strengthExpected = 2;
        int strengthActual = rogue.baseAttributes.strength;
        int dexterityExpected = 6;
        int dexterityActual = rogue.baseAttributes.dexterity;
        int intelligenceExpected = 1;
        int intelligenceActual = rogue.baseAttributes.intelligence;

        // Assert
        assertEquals(vitalityExpected, vitalityActual);
        assertEquals(strengthExpected, strengthActual);
        assertEquals(dexterityExpected, dexterityActual);
        assertEquals(intelligenceExpected, intelligenceActual);
    }

    @Test
    void init_warriorCorrectAttributesAtInit_ShouldReturn10521() {
        // Arrange
        Character warrior = new Warrior("Carl Riiber");
        int vitalityExpected = 10;
        int vitalityActual = warrior.baseAttributes.vitality;
        int strengthExpected = 5;
        int strengthActual = warrior.baseAttributes.strength;
        int dexterityExpected = 2;
        int dexterityActual = warrior.baseAttributes.dexterity;
        int intelligenceExpected = 1;
        int intelligenceActual = warrior.baseAttributes.intelligence;

        // Assert
        assertEquals(vitalityExpected, vitalityActual);
        assertEquals(strengthExpected, strengthActual);
        assertEquals(dexterityExpected, dexterityActual);
        assertEquals(intelligenceExpected, intelligenceActual);
    }

    @Test
    void levelUp_mageCorrectAttributesAtLevelUp_ShouldReturn82213() {
        // Arrange
        Character mage = new Mage("Carl Riiber");
        mage.levelUp();
        int vitalityExpected = 8;
        int vitalityActual = mage.baseAttributes.vitality;
        int strengthExpected = 2;
        int strengthActual = mage.baseAttributes.strength;
        int dexterityExpected = 2;
        int dexterityActual = mage.baseAttributes.dexterity;
        int intelligenceExpected = 13;
        int intelligenceActual = mage.baseAttributes.intelligence;

        // Assert
        assertEquals(vitalityExpected, vitalityActual);
        assertEquals(strengthExpected, strengthActual);
        assertEquals(dexterityExpected, dexterityActual);
        assertEquals(intelligenceExpected, intelligenceActual);
    }

    @Test
    void levelUp_rangerCorrectAttributesAtLevelUp_ShouldReturn102122() {
        // Arrange
        Character ranger = new Ranger("Carl Riiber");
        ranger.levelUp();
        int vitalityExpected = 10;
        int vitalityActual = ranger.baseAttributes.vitality;
        int strengthExpected = 2;
        int strengthActual = ranger.baseAttributes.strength;
        int dexterityExpected = 12;
        int dexterityActual = ranger.baseAttributes.dexterity;
        int intelligenceExpected = 2;
        int intelligenceActual = ranger.baseAttributes.intelligence;

        // Assert
        assertEquals(vitalityExpected, vitalityActual);
        assertEquals(strengthExpected, strengthActual);
        assertEquals(dexterityExpected, dexterityActual);
        assertEquals(intelligenceExpected, intelligenceActual);
    }

    @Test
    void levelUp_rogueCorrectAttributesAtLevelUp_ShouldReturn113102() {
        // Arrange
        Character rogue = new Rogue("Carl Riiber");
        rogue.levelUp();
        int vitalityExpected = 11;
        int vitalityActual = rogue.baseAttributes.vitality;
        int strengthExpected = 3;
        int strengthActual = rogue.baseAttributes.strength;
        int dexterityExpected = 10;
        int dexterityActual = rogue.baseAttributes.dexterity;
        int intelligenceExpected = 2;
        int intelligenceActual = rogue.baseAttributes.intelligence;

        // Assert
        assertEquals(vitalityExpected, vitalityActual);
        assertEquals(strengthExpected, strengthActual);
        assertEquals(dexterityExpected, dexterityActual);
        assertEquals(intelligenceExpected, intelligenceActual);
    }

    @Test
    void levelUp_warriorCorrectAttributesAtLevelUp_ShouldReturn15842() {
        // Arrange
        Character warrior = new Warrior("Carl Riiber");
        warrior.levelUp();
        int vitalityExpected = 15;
        int vitalityActual = warrior.baseAttributes.vitality;
        int strengthExpected = 8;
        int strengthActual = warrior.baseAttributes.strength;
        int dexterityExpected = 4;
        int dexterityActual = warrior.baseAttributes.dexterity;
        int intelligenceExpected = 2;
        int intelligenceActual = warrior.baseAttributes.intelligence;

        // Assert
        assertEquals(vitalityExpected, vitalityActual);
        assertEquals(strengthExpected, strengthActual);
        assertEquals(dexterityExpected, dexterityActual);
        assertEquals(intelligenceExpected, intelligenceActual);
    }
}