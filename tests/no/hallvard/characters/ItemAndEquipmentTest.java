package no.hallvard.characters;

import no.hallvard.enumerators.ArmorType;
import no.hallvard.enumerators.Slot;
import no.hallvard.enumerators.WeaponType;
import no.hallvard.exceptions.InvalidArmorException;
import no.hallvard.exceptions.InvalidWeaponException;
import no.hallvard.items.armors.Armor;
import no.hallvard.items.weapons.Weapon;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ItemAndEquipmentTest {

    @Test
    void equipWeapon_tooHighLevelWeapon_throwError(){
        // Arrange
        Warrior man = new Warrior("The Man");
        Weapon testWeapon = new Weapon("Common axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);

        // Act
        testWeapon.setLevelRequired(2);

        // Assert
        assertThrows(InvalidWeaponException.class, () -> {
            man.equipWeapon(testWeapon);
        });
    }

    @Test
    void equipArmor_tooHighLevelArmor_throwError(){
        // Arrange
        Warrior man = new Warrior("The Man");
        Armor testPlateBody = new Armor("Common Plate Body Armor", 1, Slot.BODY, new PrimaryAttribute(1, 0, 0, 2), ArmorType.PLATE);

        // Act
        testPlateBody.setLevelRequired(2);

        // Assert
        assertThrows(InvalidArmorException.class, () -> {
            man.equipArmor(testPlateBody, Slot.BODY);
        });
    }

    @Test
    void equipWeapon_wrongWeaponType_throwError(){
        // Arrange
        Warrior man = new Warrior("The Big Man");
        Weapon testBow = new Weapon("Common bow", 1, Slot.WEAPON, WeaponType.BOW, 12, 0.8);

        // Act

        // Assert
        assertThrows(InvalidWeaponException.class, () -> {
            man.equipWeapon(testBow);
        });
    }

    @Test
    void equipArmor_wrongArmorType_throwError(){
        // Arrange
        Warrior man = new Warrior("The Man");
        Armor testClothHead = new Armor("Common Cloth Head Armor", 1, Slot.HEAD, new PrimaryAttribute(0, 0, 5, 1), ArmorType.CLOTH);

        // Act

        // Assert
        assertThrows(InvalidArmorException.class, () -> {
            man.equipArmor(testClothHead, Slot.HEAD);
        });
    }

    @Test
    void equipWeapon_correctWeaponType_returnTrue(){
        // Arrange
        Warrior man = new Warrior("The Man");
        Weapon testWeapon = new Weapon("Common axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);
        boolean expected = true;
        boolean actual = false;

        // Act
        try{
            actual = man.equipWeapon(testWeapon);
        }
        catch (InvalidWeaponException e){
            e.printStackTrace();
        }

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_correctArmorType_returnTrue(){
        // Arrange
        Warrior man = new Warrior("The Man");
        Armor testPlateBody = new Armor("Common Plate Body Armor", 1, Slot.BODY, new PrimaryAttribute(1, 0, 0, 2), ArmorType.PLATE);
        boolean expected = true;
        boolean actual = false;

        // Act
        try{
            actual = man.equipArmor(testPlateBody, Slot.BODY);
        }
        catch (InvalidArmorException e){
            e.printStackTrace();
        }

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void calculateDps_correctDps_return1(){
        // Arrange
        Warrior man = new Warrior("The Man");
        double expected = 1 * (1 + (5 / 100));
        double actual;

        // Act
        actual = man.getDps();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void calculateDps_correctDpsWithWeapon_returnSome(){
        // Arrange
        Warrior man = new Warrior("The Man");
        Weapon testWeapon = new Weapon("Common axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);
        double expected = (7 * 1.1) * (1 + (5 / 100));
        double actual;

        // Act
        try {
            man.equipWeapon(testWeapon);
        }
        catch (InvalidWeaponException e){
            e.printStackTrace();
        }
        actual = man.getDps();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void calculateDps_correctDpsWithWeaponAndArmor_returnSome(){
        // Arrange
        Warrior man = new Warrior("The Man");
        Weapon testWeapon = new Weapon("Common axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);
        Armor testPlateBody = new Armor("Common Plate Body Armor", 1, Slot.BODY, new PrimaryAttribute(1, 0, 0, 2), ArmorType.PLATE);
        double expected = (7 * 1.1) * (1 + ((5+1) / 100));
        double actual;

        // Act
        try {
            man.equipWeapon(testWeapon);
            man.equipArmor(testPlateBody, Slot.BODY);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        actual = man.getDps();

        // Assert
        assertEquals(expected, actual);
    }

}
