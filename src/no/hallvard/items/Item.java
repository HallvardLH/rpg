package no.hallvard.items;

import no.hallvard.enumerators.Slot;

public abstract class Item {
    // Default properties
    protected String name;
    protected int levelRequired;
    protected Slot slot;

    // Constructor
    public Item(String name, int levelRequired, Slot slot){
        this.name = name;
        this.levelRequired = levelRequired;
        this.slot = slot;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLevelRequired(int levelRequired) {
        this.levelRequired = levelRequired;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public String getName() {
        return name;
    }

    public int getLevelRequired() {
        return levelRequired;
    }

    public Slot getSlot() {
        return slot;
    }
}
