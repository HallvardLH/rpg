package no.hallvard.items.weapons;

import no.hallvard.enumerators.Slot;
import no.hallvard.enumerators.WeaponType;
import no.hallvard.items.Item;

public class Weapon extends Item {
    // Attributes
    private WeaponType weaponType;
    private double dps;
    private double damage;
    private double attackSpeed;


    // Constructor
    public Weapon(String name, int levelRequired, Slot slot, WeaponType weaponType, double damage, double attackSpeed) {
        super(name, levelRequired, slot);
        this.weaponType = weaponType;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.dps = this.damage * this.attackSpeed;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    public void setDamage(int damage) {
        this.damage = damage;
        dps = this.damage * this.attackSpeed;
    }

    public void setAttackSpeed(int attackSpeed) {
        this.attackSpeed = attackSpeed;
        dps = this.damage * this.attackSpeed;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public double getDps() {
        return dps;
    }

    public double getDamage() {
        return damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }
}
