package no.hallvard.items.armors;

import no.hallvard.characters.PrimaryAttribute;
import no.hallvard.enumerators.ArmorType;
import no.hallvard.enumerators.Slot;
import no.hallvard.items.Item;

public class Armor extends Item {

    PrimaryAttribute attributes = new PrimaryAttribute();
    ArmorType armorType;

    // Constructor
    public Armor(String name, int levelRequired, Slot slot, PrimaryAttribute attributes, ArmorType armorType) {
        super(name, levelRequired, slot);
        this.attributes = attributes;
        this.armorType = armorType;
    }

    public PrimaryAttribute getAttributes() {
        return attributes;
    }

    public void setAttributes(PrimaryAttribute attributes) {
        this.attributes = attributes;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }
}
