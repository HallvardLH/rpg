package no.hallvard.exceptions;

import no.hallvard.enumerators.WeaponType;
import no.hallvard.items.weapons.Weapon;

public class InvalidWeaponException extends Exception {
    public  InvalidWeaponException(Weapon weapon, Class c){
        super(String.format("Your character of type %s cannot equip weapon of type %s because " +
                "level is too high or weapon type is not applicable to character", c, weapon.getWeaponType()));
    }
}
