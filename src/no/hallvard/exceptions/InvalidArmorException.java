package no.hallvard.exceptions;
import no.hallvard.items.armors.Armor;

public class InvalidArmorException extends Exception{
    public InvalidArmorException(Armor armor, Class c){
        super(String.format("Your character of type %s cannot equip armor of type %s because " +
                "level is too high or armor type is not applicable to character", c, armor.getClass()));
    }
}
