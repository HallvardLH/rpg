package no.hallvard.enumerators;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE,
}
