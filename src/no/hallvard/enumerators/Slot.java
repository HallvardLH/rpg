package no.hallvard.enumerators;

// Enumerator for slots
public enum Slot{
    HEAD,
    BODY,
    LEGS,
    WEAPON,
};