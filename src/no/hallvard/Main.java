package no.hallvard;

import no.hallvard.characters.Mage;
import no.hallvard.characters.PrimaryAttribute;
import no.hallvard.enumerators.ArmorType;
import no.hallvard.enumerators.Slot;
import no.hallvard.enumerators.WeaponType;
import no.hallvard.exceptions.InvalidArmorException;
import no.hallvard.exceptions.InvalidWeaponException;
import no.hallvard.items.armors.Armor;
import no.hallvard.items.weapons.Weapon;

public class Main {

    public static void main(String[] args) {

        Mage m = new Mage("Riiber");
        Weapon gandalfWand = new Weapon("Gandalf's wand", 1,
                Slot.WEAPON,  WeaponType.WAND, 10, 2);
        Armor invisibleCape = new Armor("Invisible cape", 1, Slot.BODY,
                new PrimaryAttribute(2, 1, 4, 3), ArmorType.CLOTH);


        System.out.println(m);

        try{
            m.equipArmor(invisibleCape, Slot.BODY);
        }
        catch (InvalidArmorException e){
            e.printStackTrace();
        }
        System.out.println(m);
    }
}
