package no.hallvard.characters;

import no.hallvard.enumerators.ArmorType;
import no.hallvard.enumerators.Slot;
import no.hallvard.enumerators.WeaponType;
import no.hallvard.exceptions.InvalidArmorException;
import no.hallvard.exceptions.InvalidWeaponException;
import no.hallvard.items.armors.Armor;
import no.hallvard.items.weapons.Weapon;

public class Ranger extends Character{

    public Ranger(String name){
        super(name, 1, 7, 1, 8);
    }

    @Override
    public void levelUp() {
        level += 1;
        baseAttributes.vitality += 2;
        baseAttributes.strength += 1;
        baseAttributes.dexterity += 5;
        baseAttributes.intelligence += 1;
        updateTotalAttributesAndDps();
    }

    @Override
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        // Throw error if level of weapon is too high
        if (weapon.getLevelRequired() > this.level){
            throw new InvalidWeaponException(weapon, this.getClass());
        }
        // Rangers can only equip bow as weapon
        if (weapon.getWeaponType() == WeaponType.BOW){
            equipment.put(Slot.WEAPON, weapon);
            updateTotalAttributesAndDps();
            return true;
        }
        else {
            throw new InvalidWeaponException(weapon, this.getClass());
        }
    }

    // Method to equip armor
    @Override
    public boolean equipArmor(Armor armor, Slot slot) throws InvalidArmorException {
        // Throw error if level of armor is too high
        if (armor.getLevelRequired() > this.level){
            throw new InvalidArmorException(armor, this.getClass());
        }
        // Rangers can only equip leather and mail as armor
        if (armor.getArmorType() == ArmorType.LEATHER || armor.getArmorType() == ArmorType.MAIL){
            equipment.put(slot, armor);
            updateTotalAttributesAndDps();
            return true;
        }
        else {
            throw new InvalidArmorException(armor, this.getClass());
        }
    }
}
