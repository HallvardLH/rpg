package no.hallvard.characters;

import no.hallvard.enumerators.ArmorType;
import no.hallvard.enumerators.Slot;
import no.hallvard.enumerators.WeaponType;
import no.hallvard.exceptions.InvalidArmorException;
import no.hallvard.exceptions.InvalidWeaponException;
import no.hallvard.items.armors.Armor;
import no.hallvard.items.weapons.Weapon;

public class Mage extends Character{

    public Mage(String name){
        super(name, 1, 1, 8, 5);
    }

    @Override
    public void levelUp() {
        level += 1;
        baseAttributes.vitality += 3;
        baseAttributes.strength += 1;
        baseAttributes.dexterity += 1;
        baseAttributes.intelligence += 5;
        updateTotalAttributesAndDps();
    }

    // Method to equip a weapon
    @Override
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        // Throw error if level of weapon is too high
        if (weapon.getLevelRequired() > this.level){
            throw new InvalidWeaponException(weapon, this.getClass());
        }
        // Mages can only equip staff and wand as weapons
        if (weapon.getWeaponType() == WeaponType.STAFF || weapon.getWeaponType() == WeaponType.WAND){
            equipment.put(Slot.WEAPON, weapon);
            updateTotalAttributesAndDps();
            return true;
        }
        else {
            throw new InvalidWeaponException(weapon, this.getClass());
        }
    }

    // Method to equip armor
    @Override
    public boolean equipArmor(Armor armor, Slot slot) throws InvalidArmorException {
        // Throw error if level of armor is too high
        if (armor.getLevelRequired() > this.level){
            throw new InvalidArmorException(armor, this.getClass());
        }
        // Mages can only equip cloth as armor
        if (armor.getArmorType() == ArmorType.CLOTH){
            equipment.put(slot, armor);
            updateTotalAttributesAndDps();
            return true;

        }
        else {
            throw new InvalidArmorException(armor, this.getClass());
        }
    }
}
