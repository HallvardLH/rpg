package no.hallvard.characters;

public class PrimaryAttribute {
    int vitality;
    int strength;
    int dexterity;
    int intelligence;

    public PrimaryAttribute(){
        this.vitality = 0;
        this.strength = 0;
        this.dexterity = 0;
        this.intelligence = 0;
    }

    public PrimaryAttribute(int vitality, int strength, int dexterity, int intelligence){
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }
}
