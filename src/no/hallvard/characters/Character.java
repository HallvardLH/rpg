package no.hallvard.characters;

import no.hallvard.enumerators.ArmorType;
import no.hallvard.enumerators.Slot;
import no.hallvard.enumerators.WeaponType;
import no.hallvard.exceptions.InvalidArmorException;
import no.hallvard.exceptions.InvalidWeaponException;
import no.hallvard.items.Item;
import no.hallvard.items.armors.Armor;
import no.hallvard.items.weapons.Weapon;

import java.util.HashMap;

// Base class for characters
public abstract class Character {
    // Default properties
    protected String name;
    protected int level;

    // Default attributes
    protected PrimaryAttribute baseAttributes = new PrimaryAttribute();
    protected PrimaryAttribute totalAttributes = new PrimaryAttribute();
    protected double dps;

    // Equipment
    HashMap<Slot, Item> equipment = new HashMap<>();

    // Constructor
    public Character(String name, int strength, int dexterity, int intelligence, int vitality){
        this.name = name;
        this.level = 1;
        this.baseAttributes.strength = strength;
        this.baseAttributes.dexterity = dexterity;
        this.baseAttributes.intelligence = intelligence;
        this.baseAttributes.vitality = vitality;
        // Update the total attributes
        updateTotalAttributesAndDps();
    }

    // Method to be implemented by child objects
    public abstract void levelUp();

    // Method to equip an item
    public abstract boolean equipWeapon(Weapon weapon) throws InvalidWeaponException;

    // Method to equip armor
    public abstract boolean equipArmor(Armor armor, Slot slot) throws InvalidArmorException;

    // Method for printing a string with the object's data
    public String toString(){
        return "Name: " + this.name +
                "\nLevel: " + this.level +
                "\nStrength: " + this.totalAttributes.strength +
                "\nDexterity: " + this.totalAttributes.dexterity +
                "\nIntelligence: " + this.totalAttributes.intelligence +
                "\nVitality: " + this.totalAttributes.vitality +
                "\nDPS: " + this.dps;
    }

    // Method for updating total attributes
    public void updateTotalAttributesAndDps(){
        int totalArmorStrength = 0;
        int totalArmorDexterity = 0;
        int totalArmorIntelligence = 0;
        int totalArmorVitality = 0;

        Weapon weapon = null;

        // Loop over all equipped armor and add its attributes to totalAttribute
        for (Slot slot : equipment.keySet()){
            if (equipment.get(slot) instanceof Armor){
                PrimaryAttribute armorAttributes = ((Armor) equipment.get(slot)).getAttributes();
                totalArmorStrength += armorAttributes.strength;
                totalArmorDexterity += armorAttributes.dexterity;
                totalArmorIntelligence += armorAttributes.intelligence;
                totalArmorVitality += armorAttributes.vitality;
            }
            else if (equipment.get(slot) instanceof Weapon){
                weapon = (Weapon) equipment.get(slot);
            }
        }

        // Update total attributes
        totalAttributes.strength = totalArmorStrength + baseAttributes.strength;
        totalAttributes.dexterity = totalArmorDexterity + baseAttributes.dexterity;
        totalAttributes.intelligence = totalArmorIntelligence + baseAttributes.intelligence;
        totalAttributes.vitality = totalArmorVitality + baseAttributes.vitality;

        // Update dps
        if (weapon != null){
            dps = weapon.getDps() * (1 + (totalAttributes.strength + totalAttributes.dexterity +
                    totalAttributes.intelligence + totalAttributes.vitality) / 100);
        }
        else {
            dps = (1 + (totalAttributes.strength + totalAttributes.dexterity +
                    totalAttributes.intelligence + totalAttributes.vitality) / 100);
        }
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public PrimaryAttribute getBaseAttributes() {
        return baseAttributes;
    }

    public PrimaryAttribute getTotalAttributes() {
        return totalAttributes;
    }

    public double getDps() {
        return dps;
    }

    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }

}
