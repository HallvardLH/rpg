package no.hallvard.characters;

import no.hallvard.enumerators.ArmorType;
import no.hallvard.enumerators.Slot;
import no.hallvard.enumerators.WeaponType;
import no.hallvard.exceptions.InvalidArmorException;
import no.hallvard.exceptions.InvalidWeaponException;
import no.hallvard.items.armors.Armor;
import no.hallvard.items.weapons.Weapon;

public class Rogue extends Character{

    public Rogue(String name){
        super(name, 2, 6, 1, 8);
    }

    @Override
    public void levelUp() {
        level += 1;
        baseAttributes.vitality += 3;
        baseAttributes.strength += 1;
        baseAttributes.dexterity += 4;
        baseAttributes.intelligence += 1;
        updateTotalAttributesAndDps();
    }

    @Override
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        // Throw error if level of weapon is too high
        if (weapon.getLevelRequired() > this.level){
            throw new InvalidWeaponException(weapon, this.getClass());
        }
        // Rogues can only equip dagger and sword as weapons
        if (weapon.getWeaponType() == WeaponType.DAGGER || weapon.getWeaponType() == WeaponType.SWORD){
            equipment.put(Slot.WEAPON, weapon);
            updateTotalAttributesAndDps();
            return true;
        }
        else {
            throw new InvalidWeaponException(weapon, this.getClass());
        }
    }

    // Method to equip armor
    @Override
    public boolean equipArmor(Armor armor, Slot slot) throws InvalidArmorException {
        // Throw error if level of armor is too high
        if (armor.getLevelRequired() > this.level){
            throw new InvalidArmorException(armor, this.getClass());
        }
        // Rogues can only equip leather and mail as armor
        if (armor.getArmorType() == ArmorType.LEATHER || armor.getArmorType() == ArmorType.MAIL){
            equipment.put(slot, armor);
            updateTotalAttributesAndDps();
            return true;
        }
        else {
            throw new InvalidArmorException(armor, this.getClass());
        }
    }
}
